

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

info = function(){
	let name = prompt("What is your name?"); 
	let age = prompt("Enter your age"); 
	let city = prompt("Enter your City");

	console.log("Hello, " +name); 
	console.log("You are " + age + " " + "years old.")
	console.log(city);
}

info();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
favoriteBand = function(){
	let favoriteBand = ["keane", "FaberDrive", "ACDC", "The Eegles", "Metallica"]
	console.log(favoriteBand);
}
favoriteBand();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

favoriteMovie = function(){
	let Movie1 = ("1. The Lord of the Rings: 3");
	let Movie1Rating = ("Rotten Tomatoes Rating: 93%")
	console.log(Movie1);
	console.log(Movie1Rating)

	let Movie2 = ("2. Slumdog Millionaire");
	let Movie2Rating = ("Rotten Tomatoes Rating: 91%")
	console.log(Movie2);
	console.log(Movie2Rating)

	let Movie3 = ("3. Gladiator");
	let Movie3Rating = ("Rotten Tomatoes Rating: 79%")
	console.log(Movie3);
	console.log(Movie3Rating)

	let Movie4 = ("4. Forrest Gump");
	let Movie4Rating = ("Rotten Tomatoes Rating: 71%")
	console.log(Movie4);
	console.log(Movie4Rating)

	let Movie5 = ("5. Mission Impossible: Fall-out");
	let Movie5Rating = ("Rotten Tomatoes Rating: 97%")
	console.log(Movie5);
	console.log(Movie5Rating)



}
favoriteMovie();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	alert("You are friends with:")
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
